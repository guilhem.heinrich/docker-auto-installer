#! /bin/bash

cat <<- EndOfMessage
	Install the latest Docker CE (for AMD processors) on a Ubuntu machine
	Following <https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce>
EndOfMessage

cat <<- EndOfMessage
	Uninstall old versions
EndOfMessage
apt-get remove docker docker-engine docker.io

cat <<- EndOfMessage
	Set up the repository
EndOfMessage
apt-get update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common \
	-y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

cat <<- EndOfMessage
	Install docker CE
EndOfMessage
apt-get update

apt-get install docker-ce docker-ce-cli containerd.io

cat <<- EndOfMessage
	`docker --version` is now installed
EndOfMessage

cat <<- EndOfMessage
	Linux post installation steps: running container as a non-root user
EndOfMessage

groupadd docker
usermod -aG docker $USER
newgrp docker